import React, { Component } from 'react'
import {
  BrowserRouter as Router,
} from 'react-router-dom'
import NavBar from './components/NavBar'
import TemporaryDrawer from './components/TemporaryDrawer'

const TemporaryDrawerState = React.createContext(false);


class App extends Component {
  state = {
    isTemporaryDrawerOpen: true,
  };

  render() {
    return (
      <Router>
        <div>
          <TemporaryDrawerState.Provider value={this.state.isTemporaryDrawerOpen}>
            <NavBar />
            <TemporaryDrawer />
          </TemporaryDrawerState.Provider>
        </div>
      </Router>
    )
  }
}


export default App;
